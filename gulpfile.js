var gulp = require('gulp');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var clean = require('gulp-clean');
var through = require('through-gulp');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');


var runSequence = require('run-sequence');
var pump = require('pump');


var DEST = 'dist';

function minified(shouldBeMinified) {
    var stream = through(function (file, encoding, callback) {
        console.log(file);
        this.push(file);

        return callback();
    });
    return stream;
}

gulp.task('build-dependencies', function (cb) {
    pump([gulp.src('app/index.html'),
            useref(),
            gulpif('*.js', uglify()),
            gulpif('*.css', minifyCss()),
            gulp.dest(DEST)
        ],
        cb);
});

gulp.task('build-resources', function (cb) {
    pump([
            gulp.src(['app/**/*','!app/js/**/*','!app/css/**/*','!app/bower_components/**/*',
                '!app/bower_components', '!app/js', '!app/css',
                '!app/index.html']),
            gulp.dest(DEST)
        ],
        cb);
});

gulp.task('build', ['build-dependencies', 'build-resources']);

gulp.task('clean', function () {
    return gulp.src('dist/*', {read: false})
        .pipe(clean());
});

gulp.task('clean-build', function (cb) {
    runSequence(
        'clean',
        'build'
        ,cb);
});

gulp.task('default', ['clean-build']);